# C19Lite

C19Lite will become available on this repository whenever it's ready.  
C19Lite is a small app gathering information from `https://thl.fi`, all data being stored is in json format.  
`C19Lite by kuzeyron` is a product meant as an CV project.
